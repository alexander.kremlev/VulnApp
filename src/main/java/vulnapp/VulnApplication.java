package vulnapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VulnApplication {

    public static void main(String[] args) {
        SpringApplication.run(vulnapp.VulnApplication.class, args);
    }

}
