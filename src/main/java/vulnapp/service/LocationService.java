package vulnapp.service;

import vulnapp.dto.LocationDto;
import vulnapp.entity.Location;

import javax.sql.DataSource;
import java.util.List;

public interface LocationService {
    void saveLocation(LocationDto locationDto);

    Location findLocationByCode(String code);

    List<LocationDto> unsafeFindLocation(String code, DataSource dataSource);

    Location findLocationByCoordinates(String coordinates);
}