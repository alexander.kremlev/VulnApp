# VulnApp

Пример выполнения SCA и SAST с последующей отправкой результатов в системы триажа.

## Примеры конвейера

### DevSecOps (trivy source + binary, semgrep, checkov, ZAP)

https://gitlab.com/alexander.kremlev/VulnApp/-/blob/DevSecOps/.gitlab-ci.yml?ref_type=heads


### SCA + SAST + Triage (CycloneDX, Semgrep, Defect  Dojo, Dependency Track)

Сборка SBOM с помощью CycloneDX, отправка результатов в Dep Track на анализ, проверка SAST с помощью Semgrep отправка результатов в DD на анализ.

https://gitlab.com/alexander.kremlev/VulnApp/-/blob/DD-DT-integration/.gitlab-ci.yml?ref_type=heads

### Проверка IaC (Checkmarx Kics)

https://gitlab.com/alexander.kremlev/VulnApp/-/blob/kics/.gitlab-ci.yml?ref_type=heads

### Horusec

https://gitlab.com/alexander.kremlev/VulnApp/-/blob/horusec/.gitlab-ci.yml?ref_type=heads

### DepScan

https://gitlab.com/alexander.kremlev/VulnApp/-/blob/depscan/.gitlab-ci.yml?ref_type=heads

### SBOM (CycloneDX)

https://gitlab.com/alexander.kremlev/VulnApp/-/blob/CDX_JAVA/.gitlab-ci.yml?ref_type=heads